**User Instructions:**

- Program asks about path with name of a file for the user defined maze
- Once path is provided program prints on the console output resolved maze
- All possible errors like not existing file, not existing route in a maze are handled
- Operations could be continued at runtime
- Maze with horizontal and vertical wrapping are handled as well


**Technical remarks:**

- The main class of this project is MazeApplication.java
- Project based on Maven structure
- Unit tests are in test folder
- Lombok removing boilerplate code is used
- Java 10 is used