package com.gentrack.model;

import lombok.*;

@Data
@AllArgsConstructor
public class Maze {

    private static final char WALL = '#';
    private static final char PASSAGE = ' ';

    private int width;
    private int height;
    private Point startPoint;
    private Point endPoint;
    private char[][] maze;

    public void convertMazeToMappingChar() {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                maze[i][j] = convertNumberToChar(maze[i][j]);
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                stringBuilder.append(maze[i][j]);
            }
            stringBuilder.append('\n');
        }
        return stringBuilder.toString().trim();
    }

    public void setValueToPoint(int x, int y, char value) {
        maze[x][y] = value;
    }

    public boolean isOpenVertically() {
        for (int i = 0; i < width; i++) {
            if (maze[0][i] == '0' || maze[height - 1][i] == '0') {
                return true;
            }
        }
        return false;
    }

    public boolean isOpenHorizontally() {
        for (int i = 0; i < height; i++) {
            if (maze[i][0] == '0' || maze[i][width - 1] == '0') {
                return true;
            }
        }
        return false;
    }

    public boolean isOpen() {
        return isOpenHorizontally() || isOpenVertically();
    }

    private char convertNumberToChar(char character) {
        if (character == '1') {
            return WALL;
        }
        if (character == '0') {
            return PASSAGE;
        }
        return character;
    }
}
