package com.gentrack.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class Point {

    @Getter
    @Setter
    private int height;
    @Getter
    @Setter
    private int width;

}
