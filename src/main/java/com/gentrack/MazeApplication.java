package com.gentrack;
/*
    @Author Szczepan Kras
 */

import com.gentrack.model.Maze;
import com.gentrack.service.MazeBuilder;
import com.gentrack.service.error.NoRouteInMazeException;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;

import static com.gentrack.FileLoader.loadFileFromPath;
import static com.gentrack.service.MazeSolver.solveWithBFS;

public class MazeApplication {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        boolean continueRuntime = true;

        while (continueRuntime) {
            try {
                String filePath = readNameAndLocationOfMazeFile();

                MazeBuilder mazeBuilder = new MazeBuilder(loadFileFromPath(Paths.get(filePath)));
                Maze maze = mazeBuilder.build();

                solveWithBFS(maze);

                System.out.println(maze);

            } catch (NoRouteInMazeException | IOException e) {
                System.out.println(e.getMessage());
            }
            System.out.println("\nDo you want to continue with other maze? (yes/no)");
            String continueChoice = scanner.nextLine();
            if (continueChoice.equals("no")) {
                continueRuntime = false;
            } else if (continueChoice.equals("yes")) {
                continueRuntime = true;
            } else {
                System.out.println("No valid input. Program clsoed :( ");
                continueRuntime = false;
            }
        }
    }

    private static String readNameAndLocationOfMazeFile() {
        System.out.println("\nPlease provide the path and name of your input file: (Example -> /Users/Gentracker/examples/inputTest.txt (unix based)");
        return scanner.nextLine();
    }
}
