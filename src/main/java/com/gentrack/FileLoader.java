package com.gentrack;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

public class FileLoader {

    private static final String ERROR_MESSAGE = "Problem with data loading occurred. Please check if specified file exists:";

    public static Stream<String> loadFileFromPath(Path path) throws IOException {
        Stream outputStream;
        try {
            outputStream = Files.lines(path);
        } catch (IOException loadFileException) {
            throw new IOException(ERROR_MESSAGE + path.getFileName());
        }
        return outputStream;
    }
}
