package com.gentrack.service;

import com.gentrack.model.Maze;
import com.gentrack.model.Point;

public class MazeWrapSolver {
    private static final int ARRAY_OFFSET = 4;
    private static final int COORDINATE_OFFSET = 2;

    private Maze maze;
    private boolean isOpenMaze;

    public MazeWrapSolver(Maze maze) {
        this.maze = maze;
        this.isOpenMaze = maze.isOpen();
    }

    public void wrapOpenedMaze() {
        if (isOpenMaze) {
            maze.setMaze(extendMazeAreaVirtually());
            reassignMaze(true);
        }
    }

    public void cleanVirtualBorder() {
        if (isOpenMaze) {
            int restoredHeight = maze.getHeight() - ARRAY_OFFSET;
            int restoredWidth = maze.getWidth() - ARRAY_OFFSET;
            char[][] restoredMaze = new char[restoredHeight][restoredWidth];

            for (int i = 0; i < restoredHeight; i++) {
                for (int j = 0; j < restoredWidth; j++) {
                    restoredMaze[i][j] = maze.getMaze()[i + COORDINATE_OFFSET][j + COORDINATE_OFFSET];
                }
            }
            maze.setMaze(restoredMaze);
            reassignMaze(false);
        }
    }

    private void reassignMaze(boolean increase) {
        int sizeOperator = increase ? ARRAY_OFFSET : -ARRAY_OFFSET;
        int coordinateOperator = increase ? COORDINATE_OFFSET : -COORDINATE_OFFSET;
        maze.setHeight(maze.getHeight() + sizeOperator);
        maze.setWidth(maze.getWidth() + sizeOperator);
        maze.setStartPoint(new Point(maze.getStartPoint().getHeight() + coordinateOperator, maze.getStartPoint().getWidth() + coordinateOperator));
        maze.setEndPoint(new Point(maze.getEndPoint().getHeight() + coordinateOperator, maze.getEndPoint().getWidth() + coordinateOperator));
    }

    private char[][] extendMazeAreaVirtually() {
        int widthExtended = maze.getWidth() + ARRAY_OFFSET;
        int heightExtended = maze.getHeight() + ARRAY_OFFSET;
        char[][] extendedMaze = new char[heightExtended][widthExtended];

        buildMazeBorderAndPassage(extendedMaze, heightExtended, widthExtended);
        rewriteToMazeExtended(extendedMaze, maze);

        return extendedMaze;
    }

    private void rewriteToMazeExtended(char[][] extendedMaze, Maze maze) {
        for (int i = 0; i < maze.getHeight(); i++) {
            for (int j = 0; j < maze.getWidth(); j++) {
                extendedMaze[i + COORDINATE_OFFSET][j + COORDINATE_OFFSET] = maze.getMaze()[i][j];
            }
        }
    }

    private void buildMazeBorderAndPassage(char[][] maze, int height, int width) {
        for (int i = 0; i < height; i++) {
            maze[i][0] = maze[i][width - 1] = '1';
            maze[i][1] = maze[i][width - 2] = '0';
        }
        for (int i = 1; i < width - 1; i++) {
            maze[0][i] = maze[height - 1][i] = '1';
            maze[1][i] = maze[height - 2][i] = '0';
        }
    }
}
