package com.gentrack.service;

import com.gentrack.model.Maze;
import com.gentrack.model.Point;
import com.gentrack.service.error.NoRouteInMazeException;

import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;

public class MazeSolver
{

    public static void solveWithBFS(Maze maze) throws NoRouteInMazeException {
        MazeWrapSolver mazeWrapSolver = new MazeWrapSolver(maze);
        mazeWrapSolver.wrapOpenedMaze();

        processBFS(maze);
        markPathAndCleanSolvedMaze(maze);

        mazeWrapSolver.cleanVirtualBorder();
        maze.convertMazeToMappingChar();
    }

    private static void processBFS(Maze maze)
    {
        Point startPoint = maze.getStartPoint();
        Point endPoint = maze.getEndPoint();

        maze.setValueToPoint(endPoint.getHeight(), endPoint.getWidth(), '0');
        Queue<Point> queue = new LinkedList<>();
        queue.add(startPoint);

        int currentRow, currentColumn;
        char[][] mazeMap = maze.getMaze();

        while (!queue.isEmpty())
        {
            currentRow = Objects.requireNonNull(queue.peek()).getHeight();
            currentColumn = Objects.requireNonNull(queue.poll()).getWidth();

            if ((currentRow == endPoint.getHeight()) && (currentColumn == endPoint.getWidth()))
                break;

            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                    if ((i != j) && (i == 0 || j == 0))
                    {
                        if (mazeMap[currentRow + i][currentColumn + j] == '0')
                        {

                            if (i == -1)
                                mazeMap[currentRow + i][currentColumn + j] = 'd';
                            else if (i == 1)
                                mazeMap[currentRow + i][currentColumn + j] = 'g';
                            else if (j == -1)
                                mazeMap[currentRow + i][currentColumn + j] = 'p';
                            else
                                mazeMap[currentRow + i][currentColumn + j] = 'l';

                            queue.add(new Point(currentRow + i, currentColumn + j));
                        }
                    }
            }
        }
    }

    private static void markPathAndCleanSolvedMaze(Maze maze) throws NoRouteInMazeException {
        Point startPoint = maze.getStartPoint();
        Point endPoint = maze.getEndPoint();

        char[][] mazeMap = maze.getMaze();
        if (mazeMap[endPoint.getHeight()][endPoint.getWidth()] == '0'){
            throw new NoRouteInMazeException();
        }
        if (mazeMap[endPoint.getHeight()][endPoint.getWidth()] != '0')
        {
            int row = endPoint.getHeight();
            int column = endPoint.getWidth();

            while ((row != startPoint.getHeight()) || (column != startPoint.getWidth()))
            {
                char c = mazeMap[row][column];
                mazeMap[row][column] = 'X';
                switch (c)
                {
                    case 'd':
                        row++;
                        break;
                    case 'g':
                        row--;
                        break;
                    case 'p':
                        column++;
                        break;
                    case 'l':
                        column--;
                        break;
                }
            }
        }

        mazeMap[endPoint.getHeight()][endPoint.getWidth()] = 'E';
        mazeMap[startPoint.getHeight()][startPoint.getWidth()] = 'S';

        for (int i = 0; i < maze.getHeight(); i++)
        {
            for (int j = 0; j < maze.getWidth(); j++)
            {
                switch (maze.getMaze()[i][j])
                {
                    case 'g':
                    case 'd':
                    case 'p':
                    case 'l':
                        maze.getMaze()[i][j] = '0';
                }
            }
        }
    }

}
