package com.gentrack.service.error;

public class NoRouteInMazeException extends Exception {
    private static final String ERROR_MESSAGE = "There is no existing route in a maze from defined start point to end point :( ";

    public NoRouteInMazeException() {
        super(ERROR_MESSAGE);
    }
}
