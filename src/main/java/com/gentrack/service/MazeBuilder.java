package com.gentrack.service;

import com.gentrack.model.Maze;
import com.gentrack.model.Point;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@AllArgsConstructor
public class MazeBuilder {

    private static final String SPACE_DELIMITER = " ";

    private Stream<String> dataStream;

    public Maze build(){

        List<String> inputArgs = dataStream.collect(Collectors.toList());

        int width = Integer.parseInt(inputArgs.get(0).trim().split(SPACE_DELIMITER)[0]);
        int height = Integer.parseInt(inputArgs.get(0).trim().split(SPACE_DELIMITER)[1]);
        int startX = Integer.parseInt(inputArgs.get(1).trim().split(SPACE_DELIMITER)[0]);
        int startY = Integer.parseInt(inputArgs.get(1).trim().split(SPACE_DELIMITER)[1]);
        int endX = Integer.parseInt(inputArgs.get(2).trim().split(SPACE_DELIMITER)[0]);
        int endY = Integer.parseInt(inputArgs.get(2).trim().split(SPACE_DELIMITER)[1]);

        char[][] loadedMaze = new char[height][width];

        List<String> mazeRows = inputArgs.stream().skip(3).collect(Collectors.toList());
        assignMazeToArray(height, width, loadedMaze, mazeRows);

        Point startPoint = new Point(startY, startX);
        Point endPoint = new Point(endY, endX);

        return new Maze(width, height, startPoint, endPoint, loadedMaze);
    }

    private void assignMazeToArray(int height, int width, char[][] loadedMaze, List<String> mazeRows)
    {
        for(int i=0; i<height; i++){
            for(int j=0; j<width; j++){
                loadedMaze[i][j] = mazeRows.get(i).trim().split(SPACE_DELIMITER)[j].charAt(0);
            }
        }
    }
}
