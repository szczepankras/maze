package com.gentrack.service;

import com.gentrack.model.Maze;
import com.gentrack.service.error.NoRouteInMazeException;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Paths;

import static com.gentrack.FileLoader.loadFileFromPath;
import static com.gentrack.service.MazeSolver.solveWithBFS;
import static org.junit.jupiter.api.Assertions.*;

class MazeSolverTest {

    @Test
    void shouldSolveAndPrintMaze() throws IOException, NoRouteInMazeException {

        String standardMaze = "src/test/java/resources/inputTest.txt";

        MazeBuilder mazeBuilder = new MazeBuilder(loadFileFromPath(Paths.get(standardMaze)));
        Maze maze = mazeBuilder.build();

        assertNotNull(maze);

        String expectedSolution =
                "##########\n" +
                        "#SXX     #\n" +
                        "# #X######\n" +
                        "# #XX    #\n" +
                        "# ##X# ###\n" +
                        "# # X# # #\n" +
                        "# # XX   #\n" +
                        "# ###X####\n" +
                        "# #  XXXE#\n" +
                        "##########";

        solveWithBFS(maze);


        assertEquals(expectedSolution, maze.toString());
    }

    @Test
    void shouldSolveAndPrintHorizontalWrappingMaze() throws IOException, NoRouteInMazeException {

        String horizontalWrappingMaze = "src/test/java/resources/inputHorizontalWrappingTest.txt";

        MazeBuilder mazeBuilder = new MazeBuilder(loadFileFromPath(Paths.get(horizontalWrappingMaze)));
        Maze maze = mazeBuilder.build();

        assertNotNull(maze);

        String expectedSolution =
                "##########\n" +
                        "XS# XXXXXX\n" +
                        "  # X#####\n" +
                        "# # X    #\n" +
                        "# ##X# ###\n" +
                        "# # X# # #\n" +
                        "# # XX   #\n" +
                        "# #  X####\n" +
                        "# #  XXXE#\n" +
                        "##########";

        solveWithBFS(maze);


        assertEquals(expectedSolution, maze.toString());
    }

    @Test
    void shouldSolveAndPrintVerticalWrappingMaze() throws IOException, NoRouteInMazeException {

        String verticalWrappingMaze = "src/test/java/resources/inputVerticalWrappingTest.txt";

        MazeBuilder mazeBuilder = new MazeBuilder(loadFileFromPath(Paths.get(verticalWrappingMaze)));
        Maze maze = mazeBuilder.build();

        assertNotNull(maze);

        String expectedSolution =
                "#X########\n" +
                        "#S#       \n" +
                        "# # ######\n" +
                        "###      #\n" +
                        "# ## # ###\n" +
                        "# #  # # #\n" +
                        "# #      #\n" +
                        "# ### ####\n" +
                        "#XXXXXXXE#\n" +
                        "#X########";

        solveWithBFS(maze);


        assertEquals(expectedSolution, maze.toString());
    }

    @Test
    void shouldThrowExceptionWhenThereIsNoRouteInMazeWithGivenData() throws IOException {

        String noRouteMaze = "src/test/java/resources/inputNoRouteMazeTest.txt";

        MazeBuilder mazeBuilder = new MazeBuilder(loadFileFromPath(Paths.get(noRouteMaze)));
        Maze maze = mazeBuilder.build();

        Throwable exception = assertThrows(NoRouteInMazeException.class, () -> solveWithBFS(maze));

        assertEquals("There is no existing route in a maze from defined start point to end point :( ", exception.getMessage());
    }

    @Test
    void shouldThrowExceptionWhenFileWithMazeDoesNotExists() {

        String fakeFile = "src/test/java/resources/lookSoAwesomeTestsIprepared.txt";

        Throwable exception = assertThrows(IOException.class, () -> loadFileFromPath(Paths.get(fakeFile)));

        assertEquals("Problem with data loading occurred. Please check if specified file exists:lookSoAwesomeTestsIprepared.txt", exception.getMessage());
    }
}