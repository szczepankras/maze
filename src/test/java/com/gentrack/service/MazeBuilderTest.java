package com.gentrack.service;

import com.gentrack.model.Maze;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Stream;

import static com.gentrack.FileLoader.loadFileFromPath;
import static org.junit.jupiter.api.Assertions.*;

class MazeBuilderTest {

    private static final String FILE_PATH = "src/test/java/resources/inputTest.txt";

    private Stream<String> stream;

    @BeforeEach
    void setup() throws IOException {
        stream = loadFileFromPath(Paths.get(FILE_PATH));
    }

    @Test
    void shouldBuildMazeObject() {

        MazeBuilder mazeBuilder = new MazeBuilder(stream);

        Maze expectedMaze = mazeBuilder.build();

        assertNotNull(expectedMaze);
    }
}