package com.gentrack.service;

import com.gentrack.model.Maze;
import com.gentrack.model.Point;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Paths;

import static com.gentrack.FileLoader.loadFileFromPath;
import static org.junit.jupiter.api.Assertions.*;

class MazeWrapSolverTest {

    private static final String FILE_PATH = "src/test/java/resources/mazeOpenVerticallyAndHorizontally.txt";
    private static final int ARRAY_OFFSET = 4;
    private static final int COORDINATE_OFFSET = 2;

    private Maze maze;
    private MazeWrapSolver mazeWrapSolver;
    private int widthInitial;
    private int heightInitial;
    private Point initialStartPoint;
    private Point initialEndPoint;

    @BeforeEach
    void setUp() throws IOException {

        MazeBuilder mazeBuilder = new MazeBuilder(loadFileFromPath(Paths.get(FILE_PATH)));
        maze = mazeBuilder.build();
        mazeWrapSolver = new MazeWrapSolver(maze);

        widthInitial = maze.getWidth();
        heightInitial = maze.getHeight();

        initialStartPoint = maze.getStartPoint();
        initialEndPoint = maze.getEndPoint();
    }

    @Test
    void wrapOpenedMazeTest() {

        String expectedMaze =
                        "11111111111111\n" +
                        "10000000000001\n" +
                        "10101111111101\n" +
                        "10101000000001\n" +
                        "10101011111101\n" +
                        "10111000000101\n" +
                        "10101101011101\n" +
                        "10101001010101\n" +
                        "10101000000101\n" +
                        "10101110111101\n" +
                        "10100000000101\n" +
                        "10101111111101\n" +
                        "10000000000001\n" +
                        "11111111111111";

        mazeWrapSolver.wrapOpenedMaze();

        assertEquals(expectedMaze, maze.toString());

        assertEquals(heightInitial + ARRAY_OFFSET, maze.getHeight());
        assertEquals(widthInitial + ARRAY_OFFSET, maze.getWidth());

        assertEquals(initialStartPoint.getHeight() + COORDINATE_OFFSET, maze.getStartPoint().getHeight());
        assertEquals(initialStartPoint.getWidth() + COORDINATE_OFFSET, maze.getStartPoint().getWidth());

        assertEquals(initialEndPoint.getHeight() + COORDINATE_OFFSET, maze.getEndPoint().getHeight());
        assertEquals(initialEndPoint.getWidth() + COORDINATE_OFFSET, maze.getEndPoint().getWidth());
    }

    @Test
    void cleanVirtualBorderTest() {
        String expectedMaze =
                        "1011111111\n" +
                        "1010000000\n" +
                        "1010111111\n" +
                        "1110000001\n" +
                        "1011010111\n" +
                        "1010010101\n" +
                        "1010000001\n" +
                        "1011101111\n" +
                        "1000000001\n" +
                        "1011111111";

        mazeWrapSolver.wrapOpenedMaze();
        mazeWrapSolver.cleanVirtualBorder();

        assertEquals(expectedMaze, maze.toString());

        assertEquals(heightInitial, maze.getHeight());
        assertEquals(widthInitial , maze.getWidth());

        assertEquals(initialStartPoint.getHeight() , maze.getStartPoint().getHeight());
        assertEquals(initialStartPoint.getWidth() , maze.getStartPoint().getWidth());

        assertEquals(initialEndPoint.getHeight() , maze.getEndPoint().getHeight());
        assertEquals(initialEndPoint.getWidth(), maze.getEndPoint().getWidth());
    }

    @Test
    void shouldNotWrapWhenMazeIsNotOpenHorizontallyOrVertically() throws IOException {
        String ordinaryMaze = "src/test/java/resources/inputTest.txt";

        MazeBuilder mazeBuilder = new MazeBuilder(loadFileFromPath(Paths.get(ordinaryMaze)));
        maze = mazeBuilder.build();

        MazeBuilder mazeBuilderForExpected = new MazeBuilder(loadFileFromPath(Paths.get(ordinaryMaze)));
        Maze expectedMaze = mazeBuilderForExpected.build();

        mazeWrapSolver = new MazeWrapSolver(maze);
        mazeWrapSolver.wrapOpenedMaze();


        assertEquals(expectedMaze.toString(), maze.toString());

        assertEquals(expectedMaze.getWidth(), maze.getHeight());
        assertEquals(expectedMaze.getHeight(), maze.getWidth());

        assertEquals(expectedMaze.getStartPoint().getHeight(), maze.getStartPoint().getHeight());
        assertEquals(expectedMaze.getStartPoint().getWidth(), maze.getStartPoint().getWidth());

        assertEquals(expectedMaze.getEndPoint().getHeight(), maze.getEndPoint().getHeight());
        assertEquals(expectedMaze.getEndPoint().getWidth(), maze.getEndPoint().getWidth());
    }
}