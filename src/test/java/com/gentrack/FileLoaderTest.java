package com.gentrack;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static com.gentrack.FileLoader.loadFileFromPath;
import static org.junit.jupiter.api.Assertions.*;

class FileLoaderTest {

    private static final String FILE_PATH = "src/test/java/resources/inputTest.txt";
    private Path path;

    @BeforeEach
    void setUp() {
        path = Paths.get(FILE_PATH);
    }

    @Test
    void loadFileFromPathTest() throws IOException {
        Stream outputStream;
        outputStream = loadFileFromPath(path);

        assertNotNull(outputStream);
    }

    @Test
    void shouldThrowLoadResourceExceptionForNotExistingFile() {
        assertThrows(IOException.class, () -> loadFileFromPath(Paths.get("fake.file")));
    }
}